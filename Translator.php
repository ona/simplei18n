<?php
namespace SimpleI18n;

include('Spyc.php');

class Translator {

  private $locale = null;
  private $translations = [];
  private $translations_path = 'config/locales';

  public function __construct($locale, $options = []) {
    $this->locale = $locale;
    if( isset($options['path']) )
      $this->translations_path = $options['path'];
  }

  public function translate($message, $options = []) {
    $locale = isset($options['locale']) ? $options['locale'] : $this->locale;
    $path = explode('.', $message);
    unset($options['locale']);

    try {
      $translation = $this->get_translation($locale, $path);
      $translation = self::replace_variables($translation, $options);
    }
    catch(MissingTranslationException $e) {
      $translation = $message;
    }

    return $translation;
  }

  public function get_translation($locale, $path) {
    // autoload locale
    if( !array_key_exists($locale, $this->translations) )
      $this->load_locale($locale);

    $current = $this->translations[$locale];

    foreach($path as $p) {
      if( isset($current[$p]) )
        $current = $current[$p]; 
      else
        throw new MissingTranslationException($locale, $path);
    }

    return $current;
  }

  private function load_locale($locale) {
    $yaml = "$this->translations_path/$locale.yml";

    if( file_exists($yaml) )
      $this->translations += spyc_load_file($yaml);
  }

  private static function replace_variables(&$translation, $options) {
    $replacements = [];

    array_walk($options, function($value, $key) use(&$replacements) {
      $replacements["%$key%"] = $value;
    });

    return str_replace(array_keys($replacements), $replacements, $translation);
  }

}

?>
