<?php
namespace SimpleI18n;

class MissingTranslationException extends \Exception {

  private $locale;
  private $path;

  public function __construct($locale, $path) {
    $this->locale = $locale;
    $this->path = $path;
  }

  public function path() {
    return $this->path;
  }

  public function locale() {
    return $this->locale;
  }

}

?>
